package org.example.reuploadhw.one.models;

import org.example.reuploadhw.one.models.Point;

import java.util.Objects;

public class Circle {
    public double radius;
    public Point center;

    public Circle(double radius, Point center) {
        this.radius = radius;
        this.center = center;
    }

    public boolean contains(Point point) {
       double distance = center.distanceTo(point);
       return distance <= radius;

}

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public Point getCenter() {
        return center;
    }

    public void setCenter(Point center) {
        this.center = center;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Circle circle = (Circle) o;
        return Double.compare(circle.radius, radius) == 0 && Objects.equals(center, circle.center);
    }

    @Override
    public int hashCode() {
        return Objects.hash(radius, center);
    }
}
