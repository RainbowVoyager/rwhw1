package org.example.reuploadhw.one;

import org.example.reuploadhw.one.models.Circle;
import org.example.reuploadhw.one.models.Point;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Point> pointList = new ArrayList<>();

        while (true) {
            System.out.println("Enter x: ");
            double x = scanner.nextDouble();
            System.out.println("Enter y: ");
            double y = scanner.nextDouble();

            pointList.add(new Point(x, y));

            System.out.println("Add more point 1-yes, 2- no");
            int choice = scanner.nextInt();
            if (choice == 2) break;
        }
        System.out.println("Enter circle radius: ");
        double radius = scanner.nextDouble();
        System.out.println("Enter circle x: ");
        double x = scanner.nextDouble();
        System.out.println("Enter circle y: ");
        double y = scanner.nextDouble();

        Circle circle = new Circle(radius, new Point(x, y));

        List<Point> pointInCirce = new ArrayList<>();
        for (int i = 0; i < pointList.size(); i++) {
            Point point = pointList.get(i);
            if (circle.contains(point)) {
                pointInCirce.add(point);

            }
            System.out.println("This point in circle" + pointInCirce);
        }

    }

}





